﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LiveChartGallery
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           
            routes.MapRoute(
               "ChartEditor",                                // Route name
               "ChartEditor/{chartType}/{chartSubType}",                       // URL with parameters
               new { controller = "Home", action = "ChartEditor", chartType = string.Empty, chartSubType = string.Empty }
              );
            routes.MapRoute(
               "HomePage",                                // Route name
               "HomePage",                       // URL with parameters
               new { controller = "Home", action = "HomePage" }
              );

            routes.MapRoute(
            name: "GDGetVisualisation",
            url: "GD/GetVisualisation/{id}",
            defaults: new { controller = "Home", action = "GetVisualisation", id = string.Empty }
             );


            routes.MapRoute(
           "Default",
           "{controller}/{action}/{id}",
           defaults: new { controller = "Home", action = "Chartdetails", id = UrlParameter.Optional }
            );
        }
    }
}
