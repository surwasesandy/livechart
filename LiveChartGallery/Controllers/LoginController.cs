﻿using LiveChartGallery.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LiveChartGallery.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string UserName, string Password)
        {
            // string pwd = ConfigurationManager.AppSettings["LoginPassword"].ToString();
            //if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(pwd) && password == pwd) {
            UserprofileModel userprofile = new UserprofileModel();
            var result = userprofile.checkUser(UserName, Password);
            if (result.Id>0) {
                Session["UserID"] = result.Id.ToString();
                Session["UserName"] = result.UserName.ToString();
                FormsAuthentication.SetAuthCookie(UserName, false);
                return RedirectToAction("Chartdetails", "Home");
            }

            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}