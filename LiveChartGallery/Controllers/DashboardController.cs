﻿using LiveChartGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using MvcContrib.Pagination;

namespace LiveChartGallery.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        public int result = 0;
        // GET: Dashboard
        public ActionResult Index(QueryContextRequestDetails queryContext)
        {

            int userprofileid = 0;
            if (Session != null)
            {
                userprofileid = Convert.ToInt32(Session["UserID"]);
            }
            if ( userprofileid > 0)
            {

                Dashboard dashboard = new Dashboard();
                //  int UserprofileId = Convert.ToInt32(Session["UserID"].ToString());

                var lstDashboard = dashboard.GetAllDashboard(userprofileid);


                queryContext.RecordCount = lstDashboard.Count();
                queryContext.RecordsPerPage = queryContext.PageSize == 0 ? 10 : queryContext.PageSize;
                queryContext.PageNumber = queryContext.PageNumber == 0 ? 1 : queryContext.PageNumber;

                int? pagenumebr;
                pagenumebr = queryContext.PageNumber;

                IEnumerable<Dashboard> Idashboard = lstDashboard.ToPagedList(pagenumebr ?? 1, queryContext.RecordsPerPage);

                var viewModel = new Dashboard();

                viewModel.Id = queryContext.Id;
                viewModel.DashboardResults = new CustomPagination<Dashboard>(Idashboard, queryContext.PageNumber, queryContext.RecordsPerPage, (int)queryContext.RecordCount);
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
         
        }
        [HttpGet]
        public ActionResult CreateDashboard()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateDashboard(Dashboard dashboard)
        {
            int UserprofileId = Convert.ToInt32(Session["UserID"].ToString());
            if (dashboard.Id==0)
             result =dashboard.SaveDashboard(dashboard, UserprofileId);
            if (result > 0)
            {
                ViewBag.Result = result;
                ViewBag.Msg = "DashBoard Created.";
                return RedirectToAction("Index");
            }
            else
                return View();
        }
        [HttpGet]
        public ActionResult EditDashboard(int Id)
        {
            Dashboard dashboard = new Dashboard();
            if (Id > 0)
                dashboard= dashboard.GetAllDashboardById(Id);
            return View(dashboard);
        }
        [HttpPost]
        public ActionResult EditDashboard(Dashboard dashboard)
        {
            //int result = 0;
            if (dashboard.Id > 0)
            {
                result = dashboard.UpdateDashboard(dashboard);
            }
            if (result > 0) { 
                ViewBag.Result = result;
            ViewBag.Msg = "DashBoard Updated.";
            }
            return RedirectToAction("Index");
        }
        public ActionResult DeleteDashboard(int Id)
        {
            Dashboard dashboard = new Dashboard();
            if (Id > 0) {
               result= dashboard.DeleteDashboard(Id);
            }
            if (result > 0) {
                ViewBag.Result = result;
                ViewBag.Msg = "DashBoard Deleted.";
            }
            return RedirectToAction("Index");
        }
        public ActionResult PreViewDashboard(int Id)
        {
            List<Dashboard> dashboard = new List<Dashboard>();
            Dashboard objDashboard = new Dashboard();
            if (Id > 0)
                dashboard = objDashboard.GetAllWidgetsbyDashboardId(Id);

          
            return View(dashboard);
        }
        public class QueryContextRequestDetails
        {
            public int Id { get; set; }
            public int RecordsPerPage { get; set; }
            public int PageNumber { get; set; }

            public int PageSize { get; set; }

            public int RecordCount { get; set; }
        }
        public ActionResult DashboardPreview(int Id)
        {
            List<Dashboard> dashboard = new List<Dashboard>();
            Dashboard objDashboard = new Dashboard();
            if (Id > 0)
                dashboard = objDashboard.GetAllWidgetsbyDashboardId(Id);
                      
            return View(dashboard);
           
        }
        public ActionResult Details(int WId)
        {
            Dashboard widgetdata = new Dashboard();
            if (WId > 0)
                widgetdata = widgetdata.GetWidgetById(WId);
            GenericAmchart genericAmchart = new GenericAmchart();
            List<SelectListItem> chrturls = new List<SelectListItem>();
            int userprofileid = 0;
            if (Session != null)
            {

                userprofileid = Convert.ToInt32(Session["UserID"]);
           
            }
            var lstcharts = genericAmchart.GetChatsDetails(userprofileid);
            foreach (var item in lstcharts)
            {
                var temp = new SelectListItem
                {
                    Value = item.embedText,
                    Text = item.ChartTitle
                };
                chrturls.Add(temp);
            }
            ViewBag.Charturls = chrturls;

            return PartialView("_Chartmodel", widgetdata);
        }
        [HttpPost]
        public ActionResult Details(Dashboard dashboard)
        {
            //int result = 0;
            if (dashboard.Id > 0)
            {
                result = dashboard.UpdateWidgetsbyId(dashboard);
            }

            return Json(new { result = result });

        }

    }
}