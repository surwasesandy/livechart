﻿using LiveChartGallery.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Data;
using System.Web.Security;
using static LiveChartGallery.Controllers.DashboardController;
using MvcContrib.Pagination;

namespace LiveChartGallery.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public static readonly int iSeriescount = 100;


        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
                return View();
        }

        [HttpGet]
        public ActionResult HomePage()
        {
                List<ChatTypeclass> chatlist = ChartDropdown();
                GenericAmchart genericAmchart = new GenericAmchart();
                genericAmchart.chattypes = chatlist;
                return View(genericAmchart);
        }

        private static List<ChatTypeclass> ChartDropdown()
        {
            List<ChatTypeclass> chatlist = new List<ChatTypeclass>();
            chatlist.Add(new ChatTypeclass { chatid = 100, chatname = "XYChart" });
            chatlist.Add(new ChatTypeclass { chatid = 200, chatname = "PieChart" });
            chatlist.Add(new ChatTypeclass { chatid = 400, chatname = "RadarChart" });
            return chatlist;
        }

        [HttpPost]
        public ActionResult HomePage(GenericAmchart genericAmchart)
        {
            // string FolderName = Path.GetDirectoryName(genericAmchart.FilePath);
            //string FileName = Path.GetFileName(genericAmchart.FilePath);
            //DataTable dt = new DataTable();
            //string excelConnectionString = string.Format("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = {0}; Extended Properties='Text;HDR=YES;FMT=Delimited(,)'", FolderName);

            //using (OleDbConnection connection = new OleDbConnection())
            //{

            //    connection.ConnectionString = excelConnectionString;
            //    string sql = "select * from [" + FileName + "]";
            //    OleDbCommand command = new OleDbCommand(sql, connection);
            //    connection.Open();
            //    using (OleDbDataAdapter ad = new OleDbDataAdapter(command))
            //    {
            //        ad.Fill(dt);
            //    }
            //    connection.Close();
            //}
            GetChartConfig(genericAmchart);
            return View("_ChartPresetation", genericAmchart);
        }

        [HttpPost]
        public JsonResult GetSecondData(int firstid)
        {
            List<ChartSeriesclass> serieslist = new List<ChartSeriesclass>();
            foreach (var value in Enum.GetValues(typeof(ChartSeries)))
            {
                serieslist.Add(new ChartSeriesclass { seriesid = (int)value, seriesname = ((ChartSeries)value).ToString() });
            }
            var result = serieslist.Where(e => e.seriesid < (firstid + iSeriescount) && e.seriesid > firstid).ToList();

            return new JsonResult { Data = result };
        }

        private static GenericAmchart GetChartConfig0(GenericAmchart genericAmchart)
        {
            
            
            string Data = "\"data\":" + genericAmchart.DataSource;

            StringBuilder ChartJson = new StringBuilder();
            if (genericAmchart.chartSubType != "StackedBar" && genericAmchart.iChartType != (int)ChartType.PieChart)
            {
                ChartJson.Append("\"colors\": {\"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},");
            }
            if (genericAmchart.iChartType == (int)ChartType.PieChart || genericAmchart.iChartType == (int)ChartType.PieChart)
            {

                if (genericAmchart.chartSubType == "Simpledonut")
                {
                    ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"ticks\": { \"disabled\": true}, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                    ChartJson.AppendLine(",\"radius\":\"" + 60 + "%\"");
                    ChartJson.AppendLine(",\"innerRadius\":\"" + 40 + "%\"");

                }
                else
                {
                    ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"alignLabels\": false,\"ticks\": { \"disabled\": true},\"labels\": { \"text\": \"{value}%\",\"radius\": \"-40%\",\"fill\": \"white\",\"fontSize\":11 }, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                }

               
                // ChartJson.AppendLine("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                if (genericAmchart.iRadius)
                {
                    ChartJson.AppendLine(",\"innerRadius\":\"" + genericAmchart.iRadiusValue + "%\"");
                }
                ChartJson.AppendLine("}]");
                //ChartJson.Append();
            }
            if (genericAmchart.iChartType == (int)ChartType.XYChart || genericAmchart.iChartType == (int)ChartType.RadarChart)
            {
                StringBuilder XAxisLable = new StringBuilder();
                StringBuilder YAxisLable = new StringBuilder();
                XAxisLable.Append("\"title\": {\"type\": \"Label\",\"text\": \"" + genericAmchart.X_AxisTitle + "\",\"propertyFields\": { }},");

                YAxisLable.Append("\"title\": {\"type\": \"Label\",\"text\": \"" + genericAmchart.Y_AxisTitle + "\",\"propertyFields\": { }},");

                if (genericAmchart.Rotate == false)
                {
                    if(genericAmchart.chartSubType == "StackedColumn")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] {"####"},StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.CatagoryMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryX}: {valueY}\"}},\"stacked\": true, \"dataFields\": {  \"valueY\": \"" + item + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }else if (genericAmchart.chartSubType == "FullStackedColumn")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.CatagoryMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryX}: {valueY}\"}}, \"stacked\":true , \"dataFields\": { \"valueYShow\": \"totalPercent\" ,\"valueY\": \"" + item + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else if (genericAmchart.chartSubType == "TwoLineseries")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.CatagoryMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"bullets\":{\"values\":[{\"type\": \"CircleBullet\",\"tooltipText\": \"{categoryX} {valueY}\"}],\"template\":{\"type\":\"Bullet\"}},\"dataFields\": {\"valueY\": \"" + item + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"} },");

                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else
                    {
                         ChartJson.Append("\"series\": [{\"colors\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"],\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueY\": \"" + genericAmchart.ValueMapping + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"}");
                       
                    }
                   
                    // ChartJson.Append("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueY\": \"" + genericAmchart.ValueMapping + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"}");
                }
                else
                {

                    //  ChartJson.Append("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");

                    if (genericAmchart.chartSubType == "StackedBar")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.ValueMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryY}: {valueX}\"}}, \"stacked\":true , \"dataFields\": { \"valueX\": \"" + item + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else if (genericAmchart.chartSubType == "FullStackedBar")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.ValueMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryY}: {valueX}\"}}, \"stacked\":true , \"dataFields\": { \"valueXShow\": \"totalPercent\" ,\"valueX\": \"" + item + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else
                    {
                        ChartJson.Append("\"series\": [{\"colors\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"],\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");
                        //ChartJson.Append("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");
                    }

                }
                if (genericAmchart.chartSubType != "StackedBar" && genericAmchart.chartSubType != "FullStackedBar" && genericAmchart.chartSubType != "StackedColumn" && genericAmchart.chartSubType != "FullStackedColumn" && genericAmchart.chartSubType != "TwoLineseries")
                {
                    if (genericAmchart.chartSubType == "SimpleLine")
                    {
                        ChartJson.Append(",\"bullets\":{\"values\":[{\"type\": \"CircleBullet\",\"tooltipText\": \"{categoryX} {valueY}\"}],\"template\":{\"type\":\"Bullet\"}}");
                    }
                    else
                    {
                        ////series Name
                        ChartJson.Append(",\"name\": \"" + genericAmchart.ValueMapping + "\"");
                        ////Common for all XYChart tooltip
                        ChartJson.AppendLine(",\"columns\":{ \"template\":{ \"type\":\"Column\",\"strokeOpacity\":0,\"tooltipText\":\"" + (genericAmchart.Rotate ? "{categoryY}:{valueX}" : "{name} {categoryX}:{valueY}") + "\",\"tooltipPosition\":\"pointer\"");
                        ////Fill color
                        //ChartJson.AppendLine(",\"propertyFields\": {\"fill\": \"color\"}");
                        ////series type Ending
                        ChartJson.Append("}}");
                    }

                
                    //// series Ending
                    ChartJson.AppendLine("}]");
                }
               

                ChartJson.AppendLine(",");
                if (genericAmchart.Rotate == false)
                {
                    
                    if (genericAmchart.chartSubType == "FullStackedColumn")
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"CategoryAxis\",");
                        if (genericAmchart.X_AxisTitle != null) {
                            ChartJson.Append(XAxisLable);
                        }
                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.CatagoryMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}],\"yAxes\": [{\"type\":\"ValueAxis\",");
                            if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                           ChartJson.Append("\"strictMinMax\": true,\"min\": 0,\"max\": 100,\"calculateTotals\": true,\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}]");
                    }
                    else
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"CategoryAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }

                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.CatagoryMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}],\"yAxes\": [{\"type\":\"ValueAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        ChartJson.Append("\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}]");
                    }

                   
                }
                else
                {
                    if (genericAmchart.chartSubType == "FullStackedBar")
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"ValueAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }

                        ChartJson.Append("\"strictMinMax\": true,\"min\": 0,\"max\": 100,\"calculateTotals\": true,\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}],\"yAxes\": [{\"type\":\"CategoryAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        //                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}]");
                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\", \"location\": 0,\"opacity\": 0}},\"minGridDistance\": 2}}]");
                    }
                    else
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"ValueAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }
                        ChartJson.Append("\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}],\"yAxes\": [{\"type\":\"CategoryAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        //                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}]");
                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\", \"location\": 0,\"opacity\": 0}},\"minGridDistance\": 2}}]");
                    }
                   
                }


            }

            if (!string.IsNullOrEmpty(Data))
            {
                ChartJson.Append(",");
                ChartJson.AppendLine(Data);
                if (genericAmchart.chartSubType == "StackedBar")
                    ChartJson.Append(",\"colors\": {\"type\": \"ColorSet\",\"list\": [\"#00dea5\",\"#11957b\",\"#215356\",\"#2c243b\",\"#263b51\",\"#1b6477\",\"#108fa1\",\"#f37a25\",\"#dd5e28\",\"#cc482a\",\"#c0392b\"]}");
            }
            ChartJson.Append(",\"exporting\": { \"menu\": { \"align\": \"right\",\"verticalAlign\": \"top\" }}");

            genericAmchart.Chart_Config = ChartJson.ToString();
            genericAmchart.SeriesType = Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType);
            genericAmchart.ChartType = Enum.GetName(typeof(ChartType), genericAmchart.iChartType);
            return genericAmchart;
        }
        private static GenericAmchart GetChartConfig(GenericAmchart genericAmchart)
        {
            
            string theme1Colors ="[\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]";
            string theme2Colors = "[\"#283250\", \"#902c2d\", \"#583471\", \"#283250\", \"#00DEA5\", \"#2F283C\"]";
            string theme3Colors = "[\"#E91E63\", \"#9C27B0\", \"#C21381\", \"#902c2d\", \"#C21381\", \"#9CA3A8\"]";
            string theme = genericAmchart.colorsarray;
            if (genericAmchart.colorsarray== "Theme1")
            {
                genericAmchart.colorsarray = theme1Colors;
            }
            else if (genericAmchart.colorsarray == "Theme2")
            {
                genericAmchart.colorsarray = theme2Colors;
            }
            else if (genericAmchart.colorsarray == "Theme3")
            {
                genericAmchart.colorsarray = theme3Colors;
            }
            string Data = "\"data\":" + genericAmchart.DataSource;

            StringBuilder ChartJson = new StringBuilder();
            if (genericAmchart.chartSubType != "StackedBar" && genericAmchart.iChartType != (int)ChartType.PieChart)
            {
                if (genericAmchart.colorsarray != null)
                {
                    genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\"[", "[").Replace("]\"", "]");
                    genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\\\"", "\"");
                    ChartJson.Append("\"colors\": {\"list\": " + genericAmchart.colorsarray + "},");
                }
                else
                {
                    ChartJson.Append("\"colors\": {\"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},");
                }
                //ChartJson.Append("\"colors\": {\"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},");
            }
            if (genericAmchart.iChartType == (int)ChartType.PieChart || genericAmchart.iChartType == (int)ChartType.PieChart)
            {
                if (genericAmchart.colorsarray != null)
                {
                    if (genericAmchart.chartSubType == "Simpledonut")
                    {
                        genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\"[", "[").Replace("]\"", "]");
                        genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\\\"", "\"");
                        //ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\":  " + genericAmchart.colorsarray + "},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"ticks\": { \"disabled\": true}, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                        ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\":  " + genericAmchart.colorsarray + "},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"alignLabels\": false,\"ticks\": { \"disabled\": true},\"labels\": { \"text\": \"{value}%\",\"fill\": \"white\",\"template\":{\"fontSize\":12} }, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                        ChartJson.AppendLine(",\"radius\":\"" + 60 + "%\"");
                        ChartJson.AppendLine(",\"innerRadius\":\"" + 40 + "%\"");
                    }
                    else
                    {
                        genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\"[", "[").Replace("]\"", "]");
                        genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\\\"", "\"");
                        ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\": " + genericAmchart.colorsarray + "},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"alignLabels\": false,\"ticks\": { \"disabled\": true},\"labels\": { \"text\": \"{value}%\",\"radius\": \"-40%\",\"fill\": \"white\",\"template\":{\"fontSize\":12} }, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");

                    }
                }
                else
                {
                    if (genericAmchart.chartSubType == "Simpledonut")
                    {
                        ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"alignLabels\": false,\"ticks\": { \"disabled\": true},\"labels\": { \"text\": \"{value}%\",\"fill\": \"white\",\"template\":{\"fontSize\":12} },\"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                        ChartJson.AppendLine(",\"radius\":\"" + 60 + "%\"");
                        ChartJson.AppendLine(",\"innerRadius\":\"" + 40 + "%\"");

                    }
                    else
                    {
                        ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"alignLabels\": false,\"ticks\": { \"disabled\": true},\"labels\": { \"text\": \"{value}%\",\"fill\": \"white\",\"template\":{\"fontSize\":12} }, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                    }
                }
                //ChartJson.AppendLine("\"series\": [{ \"colors\": { \"list\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]},\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"alignLabels\": false,\"ticks\": { \"disabled\": true},\"labels\": { \"text\": \"{value}%\",\"radius\": \"-40%\",\"fill\": \"white\",\"fontSize\":11 }, \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                // ChartJson.AppendLine("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"value\": \"" + genericAmchart.ValueMapping + "\",\"category\": \"" + genericAmchart.CatagoryMapping + "\"}");
                if (genericAmchart.iRadius)
                {
                    ChartJson.AppendLine(",\"innerRadius\":\"" + genericAmchart.iRadiusValue + "%\"");
                }
                ChartJson.AppendLine("}]");
                //ChartJson.Append();
            }
            if (genericAmchart.iChartType == (int)ChartType.XYChart || genericAmchart.iChartType == (int)ChartType.RadarChart)
            {
                StringBuilder XAxisLable = new StringBuilder();
                StringBuilder YAxisLable = new StringBuilder();
                XAxisLable.Append("\"title\": {\"type\": \"Label\",\"text\": \"" + genericAmchart.X_AxisTitle + "\",\"propertyFields\": { }},");

                YAxisLable.Append("\"title\": {\"type\": \"Label\",\"text\": \"" + genericAmchart.Y_AxisTitle + "\",\"propertyFields\": { }},");

                if (genericAmchart.Rotate == false)
                {
                    if (genericAmchart.chartSubType == "StackedColumn")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.CatagoryMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryX}: {valueY}\"}},\"stacked\": true, \"dataFields\": {  \"valueY\": \"" + item + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else if (genericAmchart.chartSubType == "FullStackedColumn")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.CatagoryMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryX}: {valueY}\"}}, \"stacked\":true , \"dataFields\": { \"valueYShow\": \"totalPercent\" ,\"valueY\": \"" + item + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else if (genericAmchart.chartSubType == "TwoLineseries")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.CatagoryMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"bullets\":{\"values\":[{\"type\": \"CircleBullet\",\"tooltipText\": \"{categoryX} {valueY}\"}],\"template\":{\"type\":\"Bullet\"}},\"dataFields\": {\"valueY\": \"" + item + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"} },");

                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else
                    {

                        if (genericAmchart.colorsarray != null)
                        {
                            genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\"[", "[").Replace("]\"", "]");
                            genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\\\"", "\"");
                            ChartJson.Append("\"series\": [{\"colors\": " + genericAmchart.colorsarray + ",\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueY\": \"" + genericAmchart.ValueMapping + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"}");
                        }
                        else
                        {
                            ChartJson.Append("\"series\": [{\"colors\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"],\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueY\": \"" + genericAmchart.ValueMapping + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"}");
                        }
                        //ChartJson.Append("\"series\": [{\"colors\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"],\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueY\": \"" + genericAmchart.ValueMapping + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"}");

                    }

                    // ChartJson.Append("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueY\": \"" + genericAmchart.ValueMapping + "\",\"categoryX\": \"" + genericAmchart.CatagoryMapping + "\"}");
                }
                else
                {

                    //  ChartJson.Append("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");

                    if (genericAmchart.chartSubType == "StackedBar")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.ValueMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryY}: {valueX}\"}}, \"stacked\":true , \"dataFields\": { \"valueX\": \"" + item + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else if (genericAmchart.chartSubType == "FullStackedBar")
                    {
                        string[] valueArray = genericAmchart.columnsvalues.Split(new string[] { "####" }, StringSplitOptions.None);

                        ChartJson.Append("\"series\":[");
                        foreach (var item in valueArray)
                        {
                            if (item == genericAmchart.ValueMapping)
                            {

                            }
                            else
                            {
                                ChartJson.Append(" {\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\",\"columns\":{\"template\":{\"type\": \"Column\",\"strokeOpacity\": 0,\"tooltipText\": \" {categoryY}: {valueX}\"}}, \"stacked\":true , \"dataFields\": { \"valueXShow\": \"totalPercent\" ,\"valueX\": \"" + item + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} },");
                            }

                        }


                        ChartJson.AppendLine("]");
                    }
                    else
                    {
                        if (genericAmchart.colorsarray != null)
                        {
                            genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\"[", "[").Replace("]\"", "]");
                            genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\\\"", "\"");
                            ChartJson.Append("\"series\": [{\"colors\":" + genericAmchart.colorsarray + ",\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");
                        }
                        else
                        {
                            ChartJson.Append("\"series\": [{\"colors\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"],\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");
                        }
                        // ChartJson.Append("\"series\": [{\"colors\": [\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"],\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");
                        //ChartJson.Append("\"series\": [{\"type\": \"" + Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType) + "\", \"dataFields\": {  \"valueX\": \"" + genericAmchart.CatagoryMapping + "\",\"categoryY\": \"" + genericAmchart.ValueMapping + "\"} ");
                    }

                }
                if (genericAmchart.chartSubType != "StackedBar" && genericAmchart.chartSubType != "FullStackedBar" && genericAmchart.chartSubType != "StackedColumn" && genericAmchart.chartSubType != "FullStackedColumn" && genericAmchart.chartSubType != "TwoLineseries")
                {
                    if (genericAmchart.chartSubType == "SimpleLine")
                    {
                        ChartJson.Append(",\"bullets\":{\"values\":[{\"type\": \"CircleBullet\",\"tooltipText\": \"{categoryX} {valueY}\"}],\"template\":{\"type\":\"Bullet\"}}");
                    }
                    else
                    {
                        ////series Name
                        ChartJson.Append(",\"name\": \"" + genericAmchart.ValueMapping + "\"");
                        ////Common for all XYChart tooltip
                        ChartJson.AppendLine(",\"columns\":{ \"template\":{ \"type\":\"Column\",\"strokeOpacity\":0,\"tooltipText\":\"" + (genericAmchart.Rotate ? "{categoryY}:{valueX}" : "{name} {categoryX}:{valueY}") + "\",\"tooltipPosition\":\"pointer\"");
                        ////Fill color
                        //ChartJson.AppendLine(",\"propertyFields\": {\"fill\": \"color\"}");
                        ////series type Ending
                        ChartJson.Append("}}");
                    }


                    //// series Ending
                    ChartJson.AppendLine("}]");
                }


                ChartJson.AppendLine(",");
                if (genericAmchart.Rotate == false)
                {

                    if (genericAmchart.chartSubType == "FullStackedColumn")
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"CategoryAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }
                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.CatagoryMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}},\"fontSize\":12}],\"yAxes\": [{\"type\":\"ValueAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        ChartJson.Append("\"strictMinMax\": true,\"min\": 0,\"max\": 100,\"calculateTotals\": true,\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}},\"fontSize\": 12}]");
                    }
                    else
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"CategoryAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }

                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.CatagoryMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}},\"fontSize\":12}],\"yAxes\": [{\"type\":\"ValueAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        ChartJson.Append("\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}},\"fontSize\": 12}]");
                    }


                }
                else
                {
                    if (genericAmchart.chartSubType == "FullStackedBar")
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"ValueAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }

                        ChartJson.Append("\"strictMinMax\": true,\"min\": 0,\"max\": 100,\"calculateTotals\": true,\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}},\"fontSize\": 12}],\"yAxes\": [{\"type\":\"CategoryAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        //                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}]");
                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\", \"location\": 0,\"opacity\": 0}},\"minGridDistance\": 2},\"fontSize\": 12}]");
                    }
                    else
                    {
                        ChartJson.Append("\"xAxes\": [{\"type\":  \"ValueAxis\",");
                        if (genericAmchart.X_AxisTitle != null)
                        {
                            ChartJson.Append(XAxisLable);
                        }
                        ChartJson.Append("\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}},\"fontSize\": 12}],\"yAxes\": [{\"type\":\"CategoryAxis\",");
                        if (genericAmchart.Y_AxisTitle != null)
                        {
                            ChartJson.Append(YAxisLable);
                        }
                        //                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\",\"opacity\": 0}}}}]");
                        ChartJson.Append("\"dataFields\": {\"category\": \"" + genericAmchart.ValueMapping + "\"},\"renderer\": {\"grid\": {\"template\": {\"type\": \"Grid\", \"location\": 0,\"opacity\": 0}},\"minGridDistance\": 2},\"fontSize\": 12}]");
                    }

                }


            }

            if (!string.IsNullOrEmpty(Data))
            {
                ChartJson.Append(",");
                ChartJson.AppendLine(Data);
                if (genericAmchart.chartSubType == "StackedBar")
                {
                    if (genericAmchart.colorsarray != null)
                    {
                        genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("[", "").Replace("]", "");
                        genericAmchart.colorsarray = genericAmchart.colorsarray.Replace("\\\"", "\"");
                        //"[\"#00DEA5\", \"#2F283C\", \"#C21381\", \"#9CA3A8\", \"#583471\", \"#57F9F9\"]";
                        if (theme=="Theme1")
                        {
                            ChartJson.Append(",\"colors\": {\"type\": \"ColorSet\",\"list\": [\"#00dea5\",\"#11957b\",\"#215356\",\"#2c243b\",\"#263b51\",\"#1b6477\",\"#108fa1\",\"#f37a25\",\"#dd5e28\",\"#cc482a\",\"#c0392b\"]}");
                        }
                        else
                        {
                            ChartJson.Append(",\"colors\": {\"type\": \"ColorSet\",\"list\": ["+ genericAmchart.colorsarray + ",\"#11957b\",\"#215356\",\"#2c243b\",\"#263b51\",\"#1b6477\",\"#108fa1\",\"#f37a25\",\"#dd5e28\",\"#cc482a\",\"#c0392b\"]}");
                        }
                       
                    }
                    else
                    {
                        ChartJson.Append(",\"colors\": {\"type\": \"ColorSet\",\"list\": [\"#00dea5\",\"#11957b\",\"#215356\",\"#2c243b\",\"#263b51\",\"#1b6477\",\"#108fa1\",\"#f37a25\",\"#dd5e28\",\"#cc482a\",\"#c0392b\"]}");
                    }
                    //ChartJson.Append(",\"colors\": {\"type\": \"ColorSet\",\"list\": [\"#00dea5\",\"#11957b\",\"#215356\",\"#2c243b\",\"#263b51\",\"#1b6477\",\"#108fa1\",\"#f37a25\",\"#dd5e28\",\"#cc482a\",\"#c0392b\"]}");
                }

            }
            ChartJson.Append(",\"exporting\": { \"menu\": { \"align\": \"right\",\"verticalAlign\": \"top\" }}");
            genericAmchart.Chart_Config = ChartJson.ToString();
            genericAmchart.SeriesType = Enum.GetName(typeof(ChartSeries), genericAmchart.iSeriesType);
            genericAmchart.ChartType = Enum.GetName(typeof(ChartType), genericAmchart.iChartType);
            return genericAmchart;
        }


        public string GetEmbeded(string Config, string type)
        {
            StringBuilder HtmlEmbeded = new StringBuilder();
            HtmlEmbeded.AppendLine("<html>");
            HtmlEmbeded.AppendLine("<head>");
            HtmlEmbeded.AppendLine("<script src=\"https:///cdn.amcharts.com/lib/4/core.js\"></script>");
            HtmlEmbeded.AppendLine("<script src=\"https:///cdn.amcharts.com/lib/4/charts.js\"></script>");
            HtmlEmbeded.AppendLine("<link href=\"https:///icassets.globaldata.com/gdic/assets/css/globaldata-font.css?v=5.09\" rel=\"stylesheet\" />");
            HtmlEmbeded.AppendLine("<link href=\"https:///icassets.globaldata.com/gdic/assets/external/font-awesome/4.7.0/css/font-awesome.css\" rel=\"stylesheet\">");
            HtmlEmbeded.AppendLine("<link href=\"https:///icassets.globaldata.com/gdic/assets/css/foundation-clean.min.css\" rel=\"stylesheet\" />");
            HtmlEmbeded.AppendLine("    <link href=\"https:///icassets.globaldata.com/gdic/assets/css/globaldata.min.css?v=5.09\" rel=\"stylesheet\" />");
            HtmlEmbeded.AppendLine("</head>");
            HtmlEmbeded.AppendLine("<body>");
            HtmlEmbeded.AppendLine("<div id=\"chartdiv\" style=\"width: 100 %; height: 400px; background-color: #FFFFFF;\"></div>");
            HtmlEmbeded.AppendLine("<script type=\"text/javascript\">");
            HtmlEmbeded.AppendLine("var chart = am4core.createFromConfig({" + Config + "}, \"chartdiv\", \"" + type + "\");");
            HtmlEmbeded.AppendLine("</script>");
            HtmlEmbeded.AppendLine("</body>");
            HtmlEmbeded.AppendLine("</html>");

            return HtmlEmbeded.ToString();
        }
        [HttpGet]
        public void DownloadFile(string Config, string type)
        {

            StringBuilder HtmlEmbeded = new StringBuilder();
            HtmlEmbeded.AppendLine("<html>");
            HtmlEmbeded.AppendLine("<head>");
            HtmlEmbeded.AppendLine("<script src=\"https:///cdn.amcharts.com/lib/4/core.js\"></script>");
            HtmlEmbeded.AppendLine("<script src=\"https:///cdn.amcharts.com/lib/4/charts.js\"></script>");
            HtmlEmbeded.AppendLine("</head>");
            HtmlEmbeded.AppendLine("<body>");
            HtmlEmbeded.AppendLine("<div id=\"chartdiv\" style=\"width: 100 %; height: 400px; background-color: #FFFFFF;\"></div>");
            HtmlEmbeded.AppendLine("<script type=\"text/javascript\">");
            HtmlEmbeded.AppendLine("var chart = am4core.createFromConfig({" + Config + "}, \"chartdiv\",");
            HtmlEmbeded.AppendLine("\"" + type + "\");");
            HtmlEmbeded.AppendLine("</script>");
            HtmlEmbeded.AppendLine("</body>");
            HtmlEmbeded.AppendLine("</html>");

            StringBuilder sb = new StringBuilder();
            // string output = "Output";
            sb.Append(HtmlEmbeded);
            sb.Append("\r\n");

            string text = sb.ToString();

            Response.Clear();
            Response.ClearHeaders();
            string filename = "Chart" + DateTime.Now.ToString();
            this.Response.AppendHeader("Content-Length", text.Length.ToString());
            this.Response.ContentType = "text/html";
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename + ".html");
            // this.Response.Flush();
            //  var st = this.Response.OutputStream;

            Response.Write(text);
            this.Response.Flush();
            this.Response.End();


        }
        [HttpPost]
        public JsonResult saveHtml(string Config, string type,string subtype,string graphname)
        {
            GenericAmchart genericAmchart = new GenericAmchart();
            genericAmchart.StrHtml=GetEmbeded( Config,  type);
            Uri uri = new Uri(Request.Url.ToString());
            string requested = uri.Scheme + Uri.SchemeDelimiter + uri.Host;
            //  string requested = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
            string key = GenericAmchart.RandomKeys(6);
            genericAmchart.embedText = "/GD/GetVisualisation/" + key;
            var tempurl = requested + genericAmchart.embedText;
            int index = genericAmchart.lsttext.FindIndex(m => m.Replace(" ", "").ToLower().Contains(subtype.ToLower()));
            string chartType = genericAmchart.lsttext[index];
            int UserprofileId = Convert.ToInt32(Session["UserID"].ToString());
            genericAmchart.SaveembeddedCode(genericAmchart.StrHtml, key, chartType, tempurl, UserprofileId, graphname);
            return Json(new { URL = genericAmchart.embedText, Key = key,urlpath= requested });
        }

        //[HttpPost]
        //public JsonResult GetVisualisation(string StrKey)
        //{
        //    GenericAmchart genericAmchart = new GenericAmchart();
        //    string key = GenericAmchart.RandomKeys(6);
        //   string strHtml= genericAmchart.GetembeddedCode(StrKey);
        //    return Json(new { element = strHtml });
        //}
        [AllowAnonymous]
        public ActionResult GetVisualisation(string id)
        {
            GenericAmchart genericAmchart = new GenericAmchart();
            string key = GenericAmchart.RandomKeys(6);
            string strHtml = genericAmchart.GetembeddedCode(id);
            // GetSomeHtml( strHtml)
            ViewBag.strHtml = strHtml;
            return Content(strHtml);
            // return Json(new { element = strHtml });
        }

        #region Chart Editor

        public ActionResult ChartEditor(string chartType, string chartSubType)
        {
            var viewModel = new ChartModel();
            var genericAmchart = new GenericAmchart();
            var type = string.Empty;
            viewModel.chartType = chartType;
            viewModel.chartSubType = chartSubType;
            viewModel.genericAmchart = genericAmchart;
            genericAmchart.chartSubType = chartSubType;
            if (chartType == ChartTypes.BarChart || chartType == ChartTypes.ColumnChart || chartType == ChartTypes.LineChart || chartType == ChartTypes.PieChart)
            {
                type = "common";
            }
            else
            {
                type = chartType;
            }

            switch (type)
            {
                case "common":
                    {
                        if (!string.IsNullOrEmpty(chartSubType))
                        {
                            viewModel.FilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/PreDefinedData/" + chartType + "/" + chartSubType + "/Data.xls");
                            FileStream fstream = new FileStream(viewModel.FilePath, FileMode.Open);
                            Aspose.Cells.Workbook Excel = new Aspose.Cells.Workbook(fstream);
                            fstream.Close();
                            DataTable dt = Excel.Worksheets[0].Cells.ExportDataTable(0, 0, Excel.Worksheets[0].Cells.MaxDataRow + 1, Excel.Worksheets[0].Cells.MaxDataColumn + 1);
                            if (dt != null)
                            {
                                var columns = dt.Rows[0].ItemArray;


                                viewModel.genericAmchart.columnsvalues += string.Join("####", columns);
                                
                               
                                for (int i = 0; i < dt.Columns.Count; i++)
                                {
                                    dt.Columns[i].ColumnName = columns[i].ToString();
                                }
                                dt.Rows[0].Delete();
                                viewModel.dataTable = dt;
                                viewModel.HtmlTable = this.ConvertDataTableToHTML(viewModel.dataTable);

                                this.PreparePreDefinedChart(chartType, chartSubType, viewModel);
                            }
                        }
                    }
                    break;
            }

            ////binding dropdown values
            List<ChatTypeclass> chatlist = ChartDropdown();
            viewModel.genericAmchart.chattypes = chatlist;



            return View(viewModel);
        }
        public void PreparePreDefinedChart(string chartType, string subType, ChartModel viewModel)
        {
            var genericAmchart = new GenericAmchart();
            genericAmchart.ChartType = chartType;
            genericAmchart.chartSubType = subType;
            genericAmchart.columnsvalues = viewModel.genericAmchart.columnsvalues;
            switch (chartType)
            {
                case ChartTypes.BarChart:
                    {
                        switch (subType)
                        {
                            case ChartTypes.SimpleBar:
                                {
                                    genericAmchart.CatagoryMapping = "OptionName";
                                    genericAmchart.ValueMapping = "Values";
                                    genericAmchart.X_AxisTitle = "Values(%)";
                                    genericAmchart.Rotate = true;
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.Stackedbar:
                                {
                                    genericAmchart.CatagoryMapping = "year";
                                    genericAmchart.ValueMapping = "Financial Services";
                                    genericAmchart.X_AxisTitle = "Deal Volume";
                                    genericAmchart.Rotate = true;
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.FullStackedBar:
                                {
                                    genericAmchart.CatagoryMapping = "Industry";
                                    genericAmchart.ValueMapping = "Positive";
                                    genericAmchart.X_AxisTitle = "Values";
                                    genericAmchart.Rotate = true;
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                        }
                    }
                    break;
                case ChartTypes.ColumnChart:
                    {
                        switch (subType)
                        {
                            case ChartTypes.SimpleColumn:
                                {
                                    genericAmchart.CatagoryMapping = "optionname";
                                    genericAmchart.ValueMapping = "Values";
                                    genericAmchart.Y_AxisTitle = "Values(%)";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.StackedColumn:
                                {
                                    genericAmchart.CatagoryMapping = "category";
                                    genericAmchart.ValueMapping = "M&A";
                                    genericAmchart.Y_AxisTitle = "Number of Deals";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.FullStackedColumn:
                                {
                                    genericAmchart.CatagoryMapping = "Weekly Dates";
                                    genericAmchart.ValueMapping = "Lay-off announced";
                                    genericAmchart.Y_AxisTitle = "Values(%)";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                        }
                    }
                    break;
                case ChartTypes.LineChart:
                    {
                        switch (subType)
                        {
                            case ChartTypes.SimpleLine:
                                {
                                    genericAmchart.CatagoryMapping = "DealValue";
                                    genericAmchart.ValueMapping = "DealCount";
                                    genericAmchart.X_AxisTitle = "Deal Value";
                                    genericAmchart.Y_AxisTitle = "Deal Count";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.SmoothedLine:
                                {
                                    genericAmchart.CatagoryMapping = "optionname";
                                    genericAmchart.ValueMapping = "Values";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.TwoLineseries:
                                {
                                    genericAmchart.CatagoryMapping = "optionname";
                                    genericAmchart.ValueMapping = "Value1";
                                    genericAmchart.X_AxisTitle = "Option Name";
                                    genericAmchart.Y_AxisTitle = "Values";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                        }
                    }
                    break;
                case ChartTypes.PieChart:
                    {
                        switch (subType)
                        {
                            case ChartTypes.SimplePie:
                                {
                                    genericAmchart.CatagoryMapping = "optionname";
                                    genericAmchart.ValueMapping = "Values";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                            case ChartTypes.Simpledonut:
                                {
                                    genericAmchart.CatagoryMapping = "category";
                                    genericAmchart.ValueMapping = "Value";
                                    viewModel.genericAmchart = this.PrepareChartConfig(genericAmchart, viewModel.dataTable);
                                }
                                break;
                        }
                    }
                    break;
            }
        }

        private static GenericAmchart RotateTheProp(GenericAmchart genericAmchart)
        {
            string temp = genericAmchart.ValueMapping;
            genericAmchart.ValueMapping = genericAmchart.CatagoryMapping;
            genericAmchart.CatagoryMapping = temp;
            return genericAmchart;
        }

        public GenericAmchart PrepareChartConfig(GenericAmchart genericAmchart, DataTable data)
        {
            var resultAmchart = new GenericAmchart();
            var jsonData = JsonConvert.SerializeObject(data);



            if (genericAmchart.ChartType == "Pie")
            {
                genericAmchart.iChartType = 200;
                genericAmchart.iSeriesType = 201;
            }
            else
            {
                genericAmchart.iChartType = 100;
                if (genericAmchart.ChartType == "Line")
                {
                    genericAmchart.iSeriesType = 105;
                }
                else
                {
                    genericAmchart.iSeriesType = 101;
                }
            }


            //genericAmchart.Rotate = true;
            if (genericAmchart.Rotate)
            {
                RotateTheProp(genericAmchart);
            }
            genericAmchart.DataSource = jsonData;
            resultAmchart = GetChartConfig(genericAmchart);
            return resultAmchart;
        }

        public string ConvertDataTableToHTML(DataTable dt)
        {
            string html = "<table class=\"table table-striped\">";
            html += "<thead><tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                html += "<th contenteditable=\"true\">" + dt.Columns[i].ColumnName + "</th>";
            }
            html += "</tr></thead>";
            html += "<tbody></tr>";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td contenteditable=\"true\">" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</tbody></table>";
            return html;
        }
        #endregion

        [HttpPost]
        public ActionResult Refresh(GenericAmchart genericAmchart)
        {
            GetChartConfig(genericAmchart);
            //string  message = RenderPartialViewToString("_ChartPresetation", genericAmchart);
            return View("_ChartPresetation", genericAmchart);
            //return new JsonResult { Data = genericAmchart };

        }
        [HttpPost]
        public JsonResult GeneratehtmlFile(GenericAmchart genericAmchart)
        {
            string filename = "Chart" + Guid.NewGuid()
                  + string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);

            string path = Server.MapPath("~/temp/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string myHtml = genericAmchart.StrHtml.ToString();
            string fileName = filename + ".html";
            string filePath = path + fileName;

            System.IO.File.WriteAllText(filePath, myHtml);
            return Json(new { msg = "success", fileLink = fileName }, JsonRequestBehavior.AllowGet);

        }

        public FileResult Downloadhtml(string fileName)
        {
            var finalPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/" + fileName);
            var contentType = "text/html";

            return this.File(finalPath, contentType, "DynamicChat_" + string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now) + ".html");

        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];
                    var htmlTable = string.Empty;
                    var jsonData = string.Empty;
                    var columnJson = string.Empty;
                    string fileName;
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fileName = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fileName = file.FileName;
                    }
                    fileName = Guid.NewGuid() + "_" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/temp/"), fileName);
                    file.SaveAs(fileName);
                    var filePath = fileName;
                    FileStream fileStream = new FileStream(filePath, FileMode.Open);
                    Aspose.Cells.Workbook Excel = new Aspose.Cells.Workbook(fileStream);
                    fileStream.Close();
                    DataTable dt = Excel.Worksheets[0].Cells.ExportDataTable(0, 0, Excel.Worksheets[0].Cells.MaxDataRow + 1, Excel.Worksheets[0].Cells.MaxDataColumn + 1);
                    if (dt != null)
                    {
                        var columns = dt.Rows[0].ItemArray;
                        var columnList = new List<string>();
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = columns[i].ToString();
                            columnList.Add(columns[i].ToString());
                        }
                        dt.Rows[0].Delete();
                        columnJson = JsonConvert.SerializeObject(columnList);
                        htmlTable = this.ConvertDataTableToHTML(dt);
                        jsonData = JsonConvert.SerializeObject(dt);

                    }
                    return Json(new { msg = "File Uploaded Successfully!", HtmlTable = htmlTable, JsonData = jsonData, Columns = columnJson });

                }
                catch (Exception ex)
                {
                    return Json(new { msg = "Error occurred. Error details: " + ex.Message });
                }
            }
            else
            {
                return Json(new { msg = "No files selected." });
            }
        }
        public FileResult DownloadSampleFile(string chartType, string chartSubType)
        {
            var finalPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/PreDefinedData/" + chartType + "/" + chartSubType + "/Data.xls");
            var contentType = "xls/xlsx";
            return this.File(finalPath, contentType, chartType + "_" + chartSubType + "_Data.xls");

        }

        public ActionResult Chartdetails(QueryContextRequestDetails queryContext)
        {
            int userprofileid = 0;
            if (Session != null)
            {
                userprofileid = Convert.ToInt32(Session["UserID"]);
            }
            if ( userprofileid > 0)
            {
                GenericAmchart genericAmchart = new GenericAmchart();
                int UserprofileId = Convert.ToInt32(Session["UserID"].ToString());
                var lstcharts = genericAmchart.GetChatsDetails(UserprofileId).Where(x => x.ChartType != "" & x.embedText != "");

                queryContext.RecordCount = lstcharts.Count();
                queryContext.RecordsPerPage = queryContext.PageSize == 0 ? 10 : queryContext.PageSize;
                queryContext.PageNumber = queryContext.PageNumber == 0 ? 1 : queryContext.PageNumber;

                int? pagenumebr;
                pagenumebr = queryContext.PageNumber;

                IList<GenericAmchart> Idashboard;
                if (queryContext.PageNumber == 1)
                {
                    Idashboard = lstcharts.Take(queryContext.RecordsPerPage).ToList();
                }
                else
                {

                    int vales = (Convert.ToInt32(pagenumebr) - 1) * queryContext.RecordsPerPage;
                    Idashboard = lstcharts.Skip(vales).Take(queryContext.RecordsPerPage).ToList();
                }

                // IEnumerable<GenericAmchart> Idashboard = lstcharts.ToPagedList(pagenumebr ?? 1, queryContext.RecordsPerPage);

                var viewModel = new GenericAmchart();

                // viewModel.Id = queryContext.Id;
                viewModel.AMchartResults = new CustomPagination<GenericAmchart>(Idashboard, queryContext.PageNumber, queryContext.RecordsPerPage, (int)queryContext.RecordCount);
                return View(viewModel);
            }
            else {
                return RedirectToAction("Index", "Login");
            }
           
        }


    }
}