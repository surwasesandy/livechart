﻿using MvcContrib.Pagination;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace LiveChartGallery.Models
{


    public class ChartSeriesclass
    {
        public int seriesid { get; set; }
        public string seriesname { get; set; }

        public int chatid { get; set; }
    }

    public class ChatTypeclass
    {
        public int chatid { get; set; }
        public string chatname { get; set; }
    }
    public enum ChartSeries
    {
        /// <summary>
        ///  XYChar 100
        /// </summary>

        ColumnSeries = 101,
        ColumnSeries3D = 102,
        ConeSeries = 103,
        CurvedColumnSeries = 104,
        LineSeries = 105,
        StepLineSeries = 106,
        OHLCSeries = 107,
        CandlestickSeries = 108,
        /// <summary>
        ///  PieChart 200
        /// </summary>
        PieSeries = 201,

        /// <summary>
        /// RadarChart
        /// </summary>
        RadarSeries = 401

    }


    public enum ChartType {
        [Display(Name = "Column")]
        XYChart = 100,
        [Display(Name = "Pie")]
        PieChart = 200,
        //MapChart=300,
        [Display(Name = "Rader")]
        RadarChart = 400
        //TreeMap=500,
        //SankeyDiagram=600,
        //GaugeChart=700,
        //SlicedChart=800,
        //Sunburst=900,
        //WordCloud=1000,
        //ForceDirectedTree=1100,
    }

    public enum XYChartAxis
    {
        CategoryAxis,
        DateAxis,
        DurationAxis,
        ValueAxis,
    }

    public static class ChartTypes
    {
        /*Main chart type*/
        public const string BarChart = "Bar";
        public const string ColumnChart = "Column";
        public const string LineChart = "Line";
        public const string PieChart = "Pie";

        /*Sub chart type*/

        public const string SimpleBar = "SimpleBar";
        public const string Stackedbar = "StackedBar";
        public const string FullStackedBar = "FullStackedBar";

        public const string SimpleColumn = "SimpleColumn";
        public const string StackedColumn = "StackedColumn";
        public const string FullStackedColumn = "FullStackedColumn";

        public const string SimpleLine = "SimpleLine";
        public const string SmoothedLine = "SmoothedLine";
        public const string TwoLineseries = "TwoLineseries";

        public const string SimplePie = "SimplePie";
        public const string Simpledonut = "Simpledonut";
    }
    public class GenericAmchart
    {
        public GenericAmchart() { }
        public string Theme { get; set; }
        public string ChartType { get; set; }
        public int iChartType { get; set; }
        public string XAxis { get; set; }
        public string YAxis { get; set; }
        public string DataSource { get; set; }
        public string SeriesType { get; set; }
        public int iSeriesType { get; set; }
        public bool Rotate { get; set; }
        public string CatagoryMapping { get; set; }
        public string ValueMapping { get; set; }
        public string FilePath { get; set; }
        public string Chart_Config { get; set; }
        public bool iRadius { get; set; }
        public int iRadiusValue { get; set; }
        [AllowHtml]
        public string StrHtml { get; set; }
        public string StrKey { get; set; }
        public string embedText { get; set; }

        public string columnsvalues { get; set; }
        public string colorsarray { get; set; }


        public string chartSubType { get; set; }
        public List<ChatTypeclass> chattypes { get; set; }

        public List<ChartSeriesclass> seriestypes { get; set; }
        public string X_AxisTitle { get; set; }

        public string Y_AxisTitle { get; set; }
        public string Token { get; set; }
        public int ChartId { get; set; }
        public string CreatedDate { get; set; }
        public string ChartTitle { get; set; }

        public IPagination<GenericAmchart> AMchartResults { get; set; }
        /*Sub chart type Text*/
        public List<string> lsttext = new List<string>() { "Simple Bar Chart", "Stacked Bar Chart", "Full Stacked Bar Chart", "Simple Column Chart", "Simple Donut Chart", "Simple Pie Chart", "Two Line Series Chart", "Smoothed Line Chart", "Simple Line Chart", "Full Stacked Column Chart", "Stacked Column Chart"};
        private static System.Random random = new System.Random();
        public static string RandomKeys(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public void SaveembeddedCode(string StrHtml, string Key, string Charttype, string url, int UserprofileId = 0, string graphname = null)
        {
            //string connection = "Data Source=52.17.181.230;Initial Catalog=ChartGallry;User ID=dmsuser;Password=c0ntent4;Application Name=SitePlatformUsers;";

            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();

            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("SaveEmbeddedCode", scon);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@token", Key));
                cmd.Parameters.Add(new SqlParameter("@HtmlText", StrHtml));
                cmd.Parameters.Add(new SqlParameter("@ChartType", Charttype));
                cmd.Parameters.Add(new SqlParameter("@EmbededUrl", url));
                cmd.Parameters.Add(new SqlParameter("@userProfileId", UserprofileId));
                cmd.Parameters.Add(new SqlParameter("@graphname", graphname));
                cmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.UtcNow.ToString("dd MMM yyyy")));
                scon.Open();
                cmd.ExecuteNonQuery();

            }
        }

        public string GetembeddedCode(string Key)
        {
            string connection = "Data Source=52.17.181.230;Initial Catalog=ChartGallry;User ID=dmsuser;Password=c0ntent4;Application Name=SitePlatformUsers;";
            DataTable dt = new DataTable();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("GetEmbeddedCode", scon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@token", Key));
                scon.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["HtmlText"].ToString();
                }
                return null;

            }

        }

        public List<GenericAmchart> GetChatsDetails(int userProfileId)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            //DataTable dt = new DataTable();
            List<GenericAmchart> lstGenericAmchart = new List<GenericAmchart>();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("GetAllCharts", scon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@userProfileId", userProfileId));
                scon.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read())
                    {
                        var genericAmchart = new GenericAmchart();
                        genericAmchart.ChartId = (int)dr["Id"];
                        genericAmchart.ChartType = dr["ChartType"].ToString();
                        genericAmchart.embedText = dr["EmbededUrl"].ToString();
                        genericAmchart.ChartTitle = dr["GraphName"].ToString();
                        genericAmchart.CreatedDate = dr["CreatedDateTime"].ToString();
                        lstGenericAmchart.Add(genericAmchart);
                    }
                return lstGenericAmchart;
            }
        }



    }
}