﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LiveChartGallery.Models
{
    public class ChartModel
    {
        public string chartType { get; set; }
        public string chartSubType { get; set; }
        public string FilePath { get; set; }
        public string HtmlTable { get; set; }
        public DataTable dataTable { get; set; }
        public GenericAmchart genericAmchart { get; set; }
    }
}