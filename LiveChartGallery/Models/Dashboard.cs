﻿using MvcContrib.Pagination;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiveChartGallery.Models
{
    public class Dashboard
    {
        public Dashboard() {
            lstDashboard = new List<Dashboard>();
          
        }
        public int Id { get; set; }

        public String Name { get; set; }

        public int Widgets_No { get; set; }

        public List<Dashboard> lstDashboard { get; set; }
        public int WidgetId { get; set; }
        public string Title { get; set; }
        public string EmbeddedUrl { get; set; }
        public string WidgetValue { get; set; }

        public string Widgetstatus { get; set; }
        public List<SelectListItem> Charturls { get; set; }

       public IPagination<Dashboard> DashboardResults { get; set; }



        public int SaveDashboard(Dashboard dashboard,int userProfileId )
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            int result = 0;
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("SaveDashboard", scon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Name", dashboard.Name));
                cmd.Parameters.Add(new SqlParameter("@WidgetNo", dashboard.Widgets_No));
                cmd.Parameters.Add(new SqlParameter("@UserProfileId", userProfileId));
                scon.Open();
                result = cmd.ExecuteNonQuery();
            }
            return result;
        }


        public Dashboard GetAllDashboardById(int id)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            //DataTable dt = new DataTable();
            Dashboard dashboard = new Dashboard();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("GetDashboardById", scon);
                cmd.Parameters.Add(new SqlParameter("@Id", id));
                cmd.CommandType = CommandType.StoredProcedure;
                scon.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read())
                    {
                        dashboard.Id = (int)dr["Id"];
                        dashboard.Name = dr["Name"].ToString();
                        dashboard.Widgets_No = (int)dr["WidgetNo"];
                    }
                return dashboard;
            }
        }

        public Dashboard    GetWidgetById(int wid)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            //DataTable dt = new DataTable();
            Dashboard dashboard = new Dashboard();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("GetWidgetById", scon);
                cmd.Parameters.Add(new SqlParameter("@WId", wid));
                cmd.CommandType = CommandType.StoredProcedure;
                scon.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read())
                    {
                        dashboard.WidgetId = (int)dr["widgetid"];
                        dashboard.Title = dr["Title"].ToString();
                        dashboard.EmbeddedUrl = dr["EmbeddedUrl"].ToString();
                        dashboard.Id = (int)dr["Dashboardid"];
                    }
                return dashboard;
            }
        }
        public List<Dashboard> GetAllWidgetsbyDashboardId(int id)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            //DataTable dt = new DataTable();
            List<Dashboard> lstdashboard = new List<Dashboard>();
 
  
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("GetAllWidgetsBYDashboardId", scon);
                cmd.Parameters.Add(new SqlParameter("@Id", id));
                cmd.CommandType = CommandType.StoredProcedure;
                scon.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read())
                    {
                        var dashboard = new Dashboard();
                        dashboard.Id = (int)dr["Id"];
                        dashboard.Name = dr["Name"].ToString();
                        dashboard.Widgets_No = (int)dr["WidgetNo"];
                        dashboard.WidgetId = (int)dr["widgetid"];
                        dashboard.Title =Convert.ToString(dr["Title"]);
                        dashboard.EmbeddedUrl = Convert.ToString(dr["EmbeddedUrl"]);
                        dashboard.WidgetValue = Convert.ToString(dr["WidgetValue"]);
                        dashboard.Widgetstatus = Convert.ToString(dr["Widgetstatus"]);
                        lstdashboard.Add(dashboard);

                    }
                return lstdashboard;
            }
        }

        public List<Dashboard> GetAllDashboard(int userprofileId)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            DataTable dt = new DataTable();
            Dashboard dashboard = new Dashboard();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("GetDashboard", scon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserProfileId", userprofileId));
                scon.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read()) {
                        var temp = new Dashboard();
                        temp.Id = (int)dr["Id"];
                        temp.Name = dr["Name"].ToString();
                        temp.Widgets_No = (int)dr["WidgetNo"];
                        dashboard.lstDashboard.Add(temp);
                    }
                return dashboard.lstDashboard;
            }
        }

        public int UpdateDashboard(Dashboard dashboard)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            //Dashboard dashboard = new Dashboard();
            int result=0;
            using (SqlConnection scon = new SqlConnection(connection))
            {
                using (SqlCommand cmd = new SqlCommand("UpdateDashboard", scon)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", dashboard.Id));
                    cmd.Parameters.Add(new SqlParameter("@Name", dashboard.Name));
                    cmd.Parameters.Add(new SqlParameter("@WidgetNo", dashboard.Widgets_No));
                    scon.Open();
                    result= cmd.ExecuteNonQuery();
                }
            }
            return result;
        }

        public int UpdateWidgetsbyId(Dashboard dashboard)
        {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            //Dashboard dashboard = new Dashboard();
            int result =0;
            using (SqlConnection scon = new SqlConnection(connection))
            {
                using (SqlCommand cmd = new SqlCommand("UpdateWidgetBYId", scon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@widgetid", dashboard.WidgetId));
                    cmd.Parameters.Add(new SqlParameter("@Title", dashboard.Title));
                    cmd.Parameters.Add(new SqlParameter("@EmbeddedUrl", dashboard.EmbeddedUrl));
                    scon.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }

        public int DeleteDashboard(int id) {
            int result = 0;
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                using (SqlCommand cmd = new SqlCommand("DeleteDashboard", scon))
                {
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    scon.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;

        }
    }
  
}