﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LiveChartGallery.Models
{
    public class UserprofileModel
    {
        public string UserName { get; set; }   

        public int Id { get; set; }

        public string EmailId { get; set; }

        public string MobileNo { get; set; }

        public UserprofileModel checkUser(string userName,string pwd) {
            string connection = ConfigurationManager.ConnectionStrings["connChartGallry"].ToString();
            UserprofileModel userprofile = new UserprofileModel();
            using (SqlConnection scon = new SqlConnection(connection))
            {
                SqlCommand cmd = new SqlCommand("ValidateUser", scon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EmailId", userName));
                cmd.Parameters.Add(new SqlParameter("@Password", pwd));
                scon.Open();
                var dr = cmd.ExecuteReader();
                while (dr.Read()) {
                    userprofile.Id = (int)dr["Id"];
                    userprofile.UserName = dr["Name"].ToString();
                }
            }
            return userprofile;
        }
    }
}