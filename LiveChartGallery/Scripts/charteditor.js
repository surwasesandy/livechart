﻿var charttype = $('#hdncharttype').val();
var hdnsubtype = $('#hdnsubtype').val();
$("#btnSubmitNew").on("click", function () {

    var valueMapping = $('#ValueMapping').val();
    var categoryMapping = $('#CatagoryMapping').val();


    if (!$('#CatagoryMapping').val() && (hdnsubtype == "SimpleBar" || hdnsubtype == "SimpleColumn" || hdnsubtype == "SimpleLine" || hdnsubtype == "SimplePie" || hdnsubtype == "Simpledonut"
        || hdnsubtype == "StackedBar" || hdnsubtype == "FullStackedBar" || hdnsubtype == "StackedColumn" || hdnsubtype == "FullStackedColumn" || hdnsubtype == "TwoLineseries")) {
        alert('Please select CatagoryMapping');
        return false;
    }
    if (!$('#ValueMapping').val() && (hdnsubtype == "SimpleBar" || hdnsubtype == "SimpleColumn" || hdnsubtype == "SimpleLine" || hdnsubtype == "SimplePie" || hdnsubtype == "Simpledonut"
       )) {
        alert('Please select ValueMapping');
        return false;
    }
    if (charttype == 'Bar') {
        valueMapping = $('#CatagoryMapping').val();
        categoryMapping = $('#ValueMapping').val();
        $('#hdnRotate').val(true);
    }

    var icharttype = 0;
    if (charttype == 'Pie') {
        iRadius = true;
        icharttype = 200;
        iRadiusValue = 60;
    }
    else if (charttype == 'Rader') {
        icharttype = 300;
    }
    else {
        icharttype = 100;
    }

    var iSeriesType = 0;
    if (charttype == 'Bar' || charttype == 'Column') {
        iSeriesType = 101;
    }
    else if (charttype == 'Line') {
        iSeriesType = 105;
    }
    else if (charttype == 'Pie') {
        iSeriesType = 201;
    }

    var genericAmchart = {
        ValueMapping: valueMapping,
        CatagoryMapping: categoryMapping,
        DataSource: $('#hdnDatasource').val(),
        Rotate: $('#hdnRotate').val(),
        iChartType: icharttype,
        iSeriesType: iSeriesType,
        chartSubType: $('#hdnsubtype').val(),
        columnsvalues: $("#hdnarrayval").val(),
        X_AxisTitle: $("#XAxisTitle").val(),
        Y_AxisTitle: $("#YAxisTitle").val(),
        colorsarray: $('#hdncolorsarray').val()
    }
    $.ajax({
        url: "/Home/HomePage",
        type: "Post",
        data: { 'genericAmchart': genericAmchart },
        success: function (data) {
            $("#DynamicChartGenerate").html(data);
            $("#DynamicChartGenerate").removeClass("hide");
            $("#btnPublishShare").removeAttr("disabled");
            $("#btnPublishShare").removeClass("hide");
        },
        error: function () {
            console.log("Error");
        }
    });
});

$("#Theme1").on("click", function () {

    $('#hdncolorsarray').val('Theme1');
    ChangeTheme();
});
$("#Theme2").on("click", function () {

    $('#hdncolorsarray').val('Theme2');
    ChangeTheme();
});
$("#Theme3").on("click", function () {

    $('#hdncolorsarray').val('Theme3');
    ChangeTheme();
});
$("#btndownload").on("click", function () {

    window.location.href = "/Home/DownloadSampleFile?chartType=" + charttype + '&chartSubType=' + hdnsubtype;
});

function ChangeTheme() {
    var valueMapping = $('#ValueMapping').val();
    var categoryMapping = $('#CatagoryMapping').val();

    if (!$('#CatagoryMapping').val() && (hdnsubtype == "SimpleBar" || hdnsubtype == "SimpleColumn" || hdnsubtype == "SimpleLine" || hdnsubtype == "SimplePie" || hdnsubtype == "Simpledonut"
     || hdnsubtype == "StackedBar" || hdnsubtype == "FullStackedBar" || hdnsubtype == "StackedColumn" || hdnsubtype == "FullStackedColumn" || hdnsubtype == "TwoLineseries")) {
        alert('Please select CatagoryMapping');
        return false;
    }
    if (!$('#ValueMapping').val() && (hdnsubtype == "SimpleBar" || hdnsubtype == "SimpleColumn" || hdnsubtype == "SimpleLine" || hdnsubtype == "SimplePie" || hdnsubtype == "Simpledonut"
       )) {
        alert('Please select ValueMapping');
        return false;
    }
    if (charttype == 'Bar') {
        valueMapping = $('#CatagoryMapping').val();
        categoryMapping = $('#ValueMapping').val();
        $('#hdnRotate').val(true);
    }

    var icharttype = 0;
    if (charttype == 'Pie') {
        iRadius = true;
        icharttype = 200;
        iRadiusValue = 60;
    }
    else if (charttype == 'Rader') {
        icharttype = 300;
    }
    else {
        icharttype = 100;
    }

    var iSeriesType = 0;
    if (charttype == 'Bar' || charttype == 'Column') {
        iSeriesType = 101;
    }
    else if (charttype == 'Line') {
        iSeriesType = 105;
    }
    else if (charttype == 'Pie') {
        iSeriesType = 201;
    }


    var genericAmchart = {
        ValueMapping: valueMapping,
        CatagoryMapping: categoryMapping,
        DataSource: $('#hdnDatasource').val(),
        Rotate: $('#hdnRotate').val(),
        iChartType: icharttype,
        iSeriesType: iSeriesType,
        chartSubType: $('#hdnsubtype').val(),
        columnsvalues: $("#hdnarrayval").val(),
        X_AxisTitle: $("#XAxisTitle").val(),
        Y_AxisTitle: $("#YAxisTitle").val(),
        colorsarray: $('#hdncolorsarray').val()
    }
    $.ajax({
        url: "/Home/HomePage",
        type: "Post",
        data: { 'genericAmchart': genericAmchart },
        success: function (data) {
            $("#DynamicChartGenerate").html(data);
            $("#DynamicChartGenerate").removeClass("hide");
            $("#btnPublishShare").removeAttr("disabled");
            $("#btnPublishShare").removeClass("hide");
        },
        error: function () {
            console.log("Error");
        }
    });
}

$("#btnPublishShare").on("click", function () {
    $.ajax({
        url: "/Home/saveHtml",
        type: "Post",
        data: { Config: $("#hchartConfig").val(), type: $("#hChartType").val(), subtype: $("#hdnsubtype").val() },
        success: function (data) {
            $("#exturl").html("Direct URL: " + data.urlpath + data.URL);
            $("#iframeurl").html("<iframe src='" + data.urlpath + data.URL + "' frameborder='0' scrolling='no' style='width:100%;height:600px;'></iframe>");
            $("#iframeurl").removeClass("hide");
        },
        error: function () {
            console.log("Error");
        }
    });
})


$("#btnsaveHtml").on("click", function () {
    $.ajax({
        url: "/Home/GetEmbeded",
        type: "Post",
        data: {},
        success: function (data) {

            $("#htmlText").empty();
            $("#htmlText").val(data.src);

        },
        error: function () {
            console.log("Error");
        }
    });
});

$('#CatagoryMapping').change(function () {
    var chartSubType = hdnsubtype;


    if (chartSubType == 'SimpleBar' || chartSubType == 'SimpleColumn' || chartSubType == 'SimpleLine' || chartSubType == 'SimplePie' || chartSubType == 'Simpledonut') {
        if ($('#CatagoryMapping').val() != "" && $('#ValueMapping').val() != "") {
            $('#Theme1').removeClass("not-active");
            $('#Theme2').removeClass("not-active");
            $('#Theme3').removeClass("not-active");
        }
    }
    else {
        if ($('#CatagoryMapping').val() != "" && typeof $('#ValueMapping').val() == 'undefined') {
            $('#Theme1').removeClass("not-active");
            $('#Theme2').removeClass("not-active");
            $('#Theme3').removeClass("not-active");
        }
    }


});
$('#ValueMapping').change(function () {
    var chartSubType = hdnsubtype;


    if (chartSubType == 'SimpleBar' || chartSubType == 'SimpleColumn' || chartSubType == 'SimpleLine' || chartSubType == 'SimplePie' || chartSubType == 'Simpledonut') {
        if ($('#CatagoryMapping').val() != "" && $('#ValueMapping').val() != "") {
            $('#Theme1').removeClass("not-active");
            $('#Theme2').removeClass("not-active");
            $('#Theme3').removeClass("not-active");
        }
    }
    else {
        if ($('#CatagoryMapping').val() != "" && typeof $('#ValueMapping').val() == 'undefined') {
            $('#Theme1').removeClass("not-active");
            $('#Theme2').removeClass("not-active");
            $('#Theme3').removeClass("not-active");
        }
    }


});