﻿



$(document).ready(function () {
    $(".pageSizeDropdown").on('change', function () {
        debugger;

        var url = $(this).attr('url');


        var pagesize = $("#pageSize option:selected").text();
        var requestURL = url;
        //if(pagesize != undefined && pagesize != ""){
        //     requestURL = url + "?PageSize=" +pagesize;
        //}




        PaginationDropDownChanged(requestURL, '1', this);
    });
});

function PaginationDropDownChanged(baseurl, pageno, control) {
    debugger;
    var totalPages = parseInt($('#totalPages').val());
    var pageSize = "";
    var pageNo = "";
    var Id="";
    var DId=$('#dashboardid').val();


    if ((document.getElementById('pageSize') != undefined) && (document.getElementById('pageSize').value != "") && (document.getElementById('pageSize').value != 0)) {
        pageSize = "&pageSize=" + $("#pageSize option:selected").val();
    }
    else if ((document.getElementById('pageSize2') != undefined) && (document.getElementById('pageSize2').value != "") && (document.getElementById('pageSize2').value != 0)) {
        pageSize = "&pageSize=" + $("#pageSize2 option:selected").val();
    }

    if (pageno != '') {
        pageNo = "?pageNumber=" + pageno;
    }
    else if ((document.getElementById('bottomPageBox') != undefined) && (document.getElementById('bottomPageBox').value != "") && (document.getElementById('bottomPageBox').value != 0) && (control == "topPageSizeTxt")) {
        if ($.isNumeric(document.getElementById('bottomPageBox').value)) {
            pageNo = "?pageNumber=" + document.getElementById('bottomPageBox').value;
        }
        else {
            document.getElementById('bottomPageBox').value = "";
            return false;
        }
    }
    else if ((document.getElementById('bottomPageBox2') != undefined) && (document.getElementById('bottomPageBox2').value != "") && (document.getElementById('bottomPageBox2').value != 0) && (control == "bottomPageSizeTxt")) {
        if ($.isNumeric(document.getElementById('bottomPageBox2').value)) {
            pageNo = "?pageNumber=" + document.getElementById('bottomPageBox2').value;
        }
        else {
            document.getElementById('bottomPageBox2').value = "";
            return false;
        }
    }

    var redirctURL = baseurl;




    if (pageNo != "") {
        redirctURL = redirctURL + pageNo;
    }




    if (pageSize != "") {
        redirctURL = redirctURL + pageSize;
    }

    if (DId!= undefined && DId != "") {
        Id="&Id="+DId;
    }
    if (Id!= undefined && Id != ""){
        redirctURL = redirctURL + Id;
    }
    if (parseInt($("#bottomPageBox").val()) == 0) {
        return false;
    }

    if (parseInt($("#bottomPageBox").val()) > totalPages) {
        var pagesizeValue = $("#bottomPageBox").get(0)
        return false;
    }
    if (parseInt($("#bottomPageBox2").val()) == 0) {
        return false;
    }

    if (parseInt($("#bottomPageBox2").val()) > totalPages) {
        var pagesizeValue2 = $("#bottomPageBox2").get(0)
        return false;
    }

    if ('URLSearchParams' in window) {
        var urlParams = new URLSearchParams(window.location.search);
    }
    else {
        var urlParams = location.search.indexOf('?') > -1 ? location.search.slice(1, location.search.length) : location.search;
    }
    if (urlParams.toString() != '') {
        urlParams = removeURLParameter(urlParams.toString(), 'Id');
        urlParams = removeURLParameter(urlParams.toString(), 'pageNumber');
        urlParams = removeURLParameter(urlParams.toString(), 'pageSize');
        if (urlParams.toString() != '') {
            redirctURL = redirctURL + '&' + urlParams.toString();
        }
    }
    //if (redirctURL.toString().indexOf("PollsSurveysDatabases") == -1) {
    //    redirctURL = SetViewType(redirctURL);
    //}

    window.location.href = redirctURL;
    return false;
}

function removeURLParameter(url, parameter) {
    var urlparts = url.indexOf('?') > -1 ? url.split('?') : url;
    if (urlparts.length >= 2) {
        var prefix = encodeURIComponent(parameter) + '=';
        var pars = url.indexOf('?') > -1 ? urlparts[1].split(/[&;]/g) : urlparts.split(/[&;]/g)
        for (var i = pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }
        url = url.indexOf('?') > -1 ? urlparts[0] + '?' + pars.join('&') : pars.join('&');
        return url;
    } else {
        return url;
    }
}
