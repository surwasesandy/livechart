﻿$(function () {
    $(document).on("click", "#btnsave", function () {
        $.ajax({
            url: "/Home/saveHtml",
            type: "Post",
            data: { StrHtml: $("#htmlText").val() },
            success: function (data) {
                $("#EmbededKey").val(data.Key);
                $("#embedText").val("<iframe src='" + data.urlpath + data.URL + "' frameborder='0' scrolling='no' style='width:100%;height:600px;'>");
                $("#externallink").attr("href", data.URL);
                $("#externallink span").html(data.URL);
         
            },
            error: function () {
                console.log("Error");
            }
        });
    });
});


$(function () {
    $(document).on('click', '.clipboard-tooltip', function () {
        var btns = document.querySelectorAll('.clipboard-tooltip');
        var clipboardDemos = new ClipboardJS(btns);
        clipboardDemos.on('success', function (e) {
            e.clearSelection();
            showTooltip(e.trigger, 'Copied!');
        });
        clipboardDemos.on('error', function (e) {
            showTooltip(e.trigger, fallbackMessage(e.action));
        });
    });
});

////Table Edit 
    $(document).ready(function () {

        const $tableID = $('#csvToTable');
        const $BTN = $('#export-btn');
        $(document).on("click", "#export-btn", function () {
            const $rows = $tableID.find('tr:not(:hidden)');
            const headers = [];
            const data = [];
            // Get the headers (add special header logic here)
            $($rows[0]).find('th:not(:empty)').each(function () {
                headers.push($(this).text());
            });

            // Turn all existing rows into a loopable array
            $rows.each(function () {
                const $td = $(this).find('td');
                const h = {};
                // Use the headers from earlier to name our hash keys
                headers.forEach(function (header, i) {
                    h[header] = $td.eq(i).text();
                });

                data.push(h);
            });
            data.shift();
            console.log(data);
            var json = JSON.stringify(data);
            json = json.replace(/\\n/g, "");
            console.log(json);
            $('#hTableJson').val(json);
            //       var genericAmchart = {
            //    DataSource: JSON.stringify(data),
            //    iChartType: $("#iChartType").val(),

            //}
            $.ajax({
                url:"/Home/Refresh",
                type: "Post",
                data: $('form').serialize(),
                dataType: 'html',
                success: function (data) {
                    debugger;
                    if (data != null) {
                        $("#ChartPresentation").html(data);
                    }
                },
                error: function () {
                    console.log("Error");
                }
            });

        });
    });
    function chartDispose(DivId) {
        ////dispose chart object if already present
        if (DivId != undefined && DivId != '') {

            var charts = am4core.registry.baseSprites;
            if (charts) {
                for (var i = 0; i < charts.length; i++) {
                    if (charts[i].htmlContainer.id == DivId) {
                        charts[i].dispose();
                    }
                }
            }
            //if (am4core.registry.baseSprites.find(c => c.htmlContainer.id === DivId) != undefined) {
            //    var ch = am4core.registry.baseSprites.find(c => c.htmlContainer.id === DivId)
            //    ch.dispose();
            //    ch = null;
            //}
        }
    }